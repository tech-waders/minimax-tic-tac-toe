# MiniMax Tic Tac Toe

Simple tic-tac-toe, written in Golang. Minimax implemented.

# Using
## Prerequisites
- Golang set up on your computer

## Steps
- Download both files
- In shell (or CMD) navigate to the folder
- Type 'run main.go' 
- Press enter

(Playing instructions included)

# Version 1.1.0
## UPDATES

- Changed wording, added clearer instructions, changed starting player

## Features

- Minimax implemented
- 1-9 TL to BR numbering

## Coming soon

- Better interface
- Play again
